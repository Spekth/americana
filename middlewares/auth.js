import tokenService from '../services/token'

export default {
    // Verifica que el usuario este auntenticado de manera correcta
    verifyUsuario: async(req,res,next)=>{
      if(!req.headers.token){
        return res.status(404).send({
            message:'No token'
        });
      }
      const response = await tokenService.decode(req.headers.token);
      if(response.role == "ADMIN" || response.role == "USER"){
        next();  
      } else {
        return res.status(403).send({
           message: ' No autorizado'
        });
      }  
    },
    // Verificar si el usaurio es admin
    verifyAdministrador: async(req,res,next)=> {
        if(!req.headers.token){
            return res.status(404).send({
                message:'No token'
            });
            }
            const response = await tokenService.decode(req.headers.token);
            if(response.role == "ADMIN"){
            next();  
            } else {
            return res.status(403).send({
                message: ' No autorizado'
            });
        }
    },
    //verificar si es un usuario sin privilegios
    verifyUserSinPrivilegios: async(req,res,next)=>{
        if(!req.headers.token){
            return res.status(404).send({
                message:'No token'
            });
          }
          const response = await tokenService.decode(req.headers.token);
          // || ADMIN 
          if(response.role == "USER"){
            next();  
          } else {
            return res.status(403).send({
               message: ' No autorizado'
            });
        }
    }
    
}