import jwt from 'jsonwebtoken'
import Usuario from '../models/usuario'

// VERIFICA QUE EL TOKEN EXISTA... 
async function checkToken(token){
  let __id = null;
  try{
    const {_id } = await jwt.decode(token);
    __id = _id;   
  }catch(err){
    return false; 
  } 

  const user = await Usuario.findOne({_id:__id});
  if(user){
    const token = jwt.sign({_id:__id},'clavesecretaparagenerartoken',{expiresIn: '1d'});
    return { token,role:user.role };  
  }
}

export default {
    // Esta función crea el token
    encode: async (_id,role,correo,nombre)=>{
       const token = jwt.sign({_id:_id,role:role,correo:correo,nombre:nombre},'clavesecretaparagenerartoken',{expiresIn:'8d'});
       return token;

    },
    // Esta función recibe el token y lo decodifica 
    decode: async (token) => {
       try{
         const {_id} = await jwt.verify(token,'clavesecretaparagenerartoken');
         const user = await Usuario.findOne({_id});
         if(user){
            return user;
         }else{
            return false;
         }
       }catch(err){
         const newToken = await checkToken(token);
         return newToken;
       }
    }
}