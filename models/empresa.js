import mongoose, {Schema } from 'mongoose'

const empresaSchema = new Schema({
    nombre: {type:String, required: [true,'El nombre de la empresa es requerido']},
    nit: {type:Number, required:true},
    email: {type:String,default:'notificiacionesmon@americana.edu.co'},
    celular: {type:String,default:3136825373},
    direccion: {type:String,default:'calle 24 N 5-28'},
    logo: {type:String,default:'https://res.cloudinary.com/dqhme1rod/image/upload/v1622058076/americana/vz9ay9gxvjdrsyqtbxnf.jpg',required:false},
    state:{type:Boolean,default:true},
    createAt:{type:Date,default:Date.now()}
},{versionKey:false})

const Empresa = mongoose.model('Empresa',empresaSchema)

export default Empresa 