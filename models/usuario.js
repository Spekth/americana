import mongoose, { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator'

const rolesValidos = {
  values: ['ADMIN','USER'],
  message: '{VALUE} is not a valid role'
}
const UsuarioSchema = new Schema({
  nombre: {type:String},
  apellido: {type: String},
  nombreUsuario: {type:String},
  correo: {type:String},
  password: {type:String},
  role: {type: String,default:'USER',enum:rolesValidos},
  cedula: {type:Number,unique:true},
  image: {type:String,default:"https://res.cloudinary.com/dqhme1rod/image/upload/v1622644109/americana/im798gjpjstb06es7phf.png"},
  state:{type:Boolean,default:true},
  createAt: {type:Date, default:Date.now()} 

},{versionKey:false})  

UsuarioSchema.plugin(uniqueValidator,{mesage:'{PATH} must be unique'})
const Usuario = mongoose.model('Usuario',UsuarioSchema);
export default Usuario;

