import mongoose, { Schema } from 'mongoose';
const ReporteSchema = new Schema({
   loan: {type:Schema.Types.ObjectId, ref:'Loan'},
   state:{type:Boolean,default:true},
   createAt:{type:Date,default:Date.now()}
},{versionKey:false});

const Reporte = mongoose.model('Reporte',ReporteSchema);
export default Reporte;
