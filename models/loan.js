import mongoose, { Schema } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

let state = {
  values:['active','finished'],
  message:'{VALUE} no es un disponibilidad válida'
}
const loanSchema = new Schema({
   usuario: {type:Schema.Types.ObjectId, ref:'Usuario'},
   equipo: {type:Schema.Types.ObjectId, ref:'Equipo'},
   encargado: {type:Schema.Types.ObjectId, ref:'Encargado'},
   inicioPrestamo: {type:Date},
   finPrestamo: {type:Date},
   descripcion: {type:String},
   //  new property
   loanState: {type:String,default:'active',enum:state},
   state:{type:Boolean,default:true},
   createAt:{type:Date,default:Date.now()}
},{versionKey:false})

loanSchema.plugin(uniqueValidator,{message:'Error el {PATH} debe ser único'})
const Loan = mongoose.model('Loan',loanSchema);
export default Loan; 