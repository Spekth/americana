import mongoose, {Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
const cargoSchema = new Schema({
  nombreCargo: {type:String},
  empleados:[{
    nombre: { type:String },
    apellido: {type:String }, 
    cedula: {type:Number, required:[true,'La cedula es obligatoria']} 
  }],
  state:{type:Boolean,default:true},
  createAt:{type:Date,default:Date.now()}
})

cargoSchema.plugin(uniqueValidator,{message:'Error el {PATH} debe ser único'})

const Cargo = mongoose.model('Cargo',cargoSchema); 

export default Cargo 

