import mongoose, { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator'

const AreaSchema = new Schema ({
    nombre: { type: String},
    // El encargado es el jefe de cada area 
    encargado: {type: Schema.Types.ObjectId, ref: 'Encargado'},
    state:{type:Boolean,default:true},
    createAt:{type:Date,default:Date.now()}
},{versionKey:false});


AreaSchema.plugin(uniqueValidator,{message:'Error el {PATH} debe ser unico'})
const Area = mongoose.model('Area',AreaSchema);
export default Area;
