import mongoose, { Schema } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
const disponibility = {
  values:['true','false'],
  message:'{VALUE} no es un disponibilidad válida'
};
const estadosValidos = {
  values:['ACTIVO','INACTIVO'],
  message:'{VALUE} no es un estado válido'
}


const EquipoSchema = new Schema({
  nombre: {type:String,required:[true,'El nombre es obligatorio']},
  numSerie: {type:String,unique:true,required:[true,'El número de serie es obligatorio']},
  // // El estado[ACTIVO, DESACTIVO] 
  estado: {type:String,required:[true,'El estado es obligatorio'],default:'ACTIVO',enum:estadosValidos},
  // Si la disponibilidad esta en true, el equipo esta disponible para prestamo. 
  fechaIngreso: {type:String,required:[true,'La fecha de ingreso es obligatoria']}, 
  fechaEgreso: {type:String},  
  // disponibilidad: {type:Boolean,enum:disponibility}
  disponibilidad: {type:String,enum:disponibility},
  image: {type:String},
  state:{type:Boolean,default:true},
  createAt:{type:Date,default:Date.now()}
},{versionKey:false})


EquipoSchema.plugin(uniqueValidator,{message:'Error el {PATH} debe ser único'})
const Equipo = mongoose.model('Equipo',EquipoSchema);  
export default Equipo; 

