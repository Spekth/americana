import Empresa from '../models/empresa'
export default { 
   addEmpresa: async (req,res,next)=>{
     try {
        const body = req.body
        const empresa = await Empresa.create(body)
        res.status(200).send({empresa})
     }catch(err){
       res.status(500).send({
         message:`An error ocurred ${err}`
       })
       next(err)
     };
   },
   
   getEmpresas: async (req,res,next)=>{
    
      try{
        const empresas = await Empresa.find({})
       
        res.status(200).send({
           ok:true,
           empresas
        })
      } catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err) 
      }
   },
   
   getEmpresa: async(req,res,next)=>{
     try{
       const { id } = req.params
       const empresaID = await Empresa.findById(id)
       if(!empresaID){
        res.status(404).send({
          message:'No existe la empresa'
        })
       }else{
         res.status(200).json(empresaID)
       }

     }catch(err) {
      res.status(500).send({
        message:'Ocurrio un error '
      })
      next(err)
     }

   },
   updateEmpresa: async(req,res,next)=>{
     try{
       const { id } =  req.params;
       const update = req.body;
       const empresa = await Empresa.findByIdAndUpdate(id,update,{new:true})
       res.status(200).json(empresa)
      }catch(err){
       res.status(500).send({
         message:'Ocurrio un error'
       })
       next(err)
     }
   },
   deleteEmpresa: async(req,res,next)=>{
     try{
       const { id } = req.params
       const empresa= await Empresa.findByIdAndDelete(id)
       res.status(200).json({ok:true,message:'Empresa eliminada con exito'})
     }catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)
     }
   }
}