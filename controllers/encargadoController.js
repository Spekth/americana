import Encargado from '../models/encargado'

export default { 
  // Create Encargado
   addEncargado: async (req,res,next)=>{
     try {
        const body = req.body
        const encargado = await Encargado.create(body)
        res.status(200).send({encargado})
     }catch(err){
       res.status(500).send({
         message:`An error ocurred ${err}`
       })
       next(err)
     };
   },
   // Todos los encargados
   getEncargados: async (req,res,next)=>{
      try{
        const encargados = await Encargado.find({})
        res.status(200).send({
           ok:true,
           encargados
        })
      } catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err) 
      }
   },
   // Obtener un encargado  
   getEncargado: async(req,res,next)=>{
     try{
       const { id } = req.params
       const encargadoID = await Encargado.findById(id)
        .populate('Equipo')
       if(!encargadoID){
        res.status(404).send({
          message:'No existe el encargado'
        })
       }else{
         res.status(200).json(encargadoID)
       }
     }catch(err) {
      res.status(500).send({
        message:'Ocurrio un error '
      })
      next(err)
     }
   },
   updateEncargado: async(req,res,next)=>{
     try{
       const { id } =  req.params;
       const update = req.body;
       const encargado = await Encargado.findByIdAndUpdate(id,update,{new:true})
       res.status(200).json(encargado)
      }catch(err){
       res.status(500).send({
         message:'Ocurrio un error'
       })
       next(err)
     }
   },
   /*Los encargados no se van a eliminar, unicamente se va
   a cambiar su estado a false;*/
   deleteEncargado: async(req,res,next)=>{
     try{
       const { id } = req.params
       const encargado= await Encargado.findByIdAndDelete(id)
      // const encargado = await Encargado.findByIdAndUpdate(id,{estado:false});
       res.status(200).json({ok:true,message:'Encargado eliminado con exito'})
     }catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)
     }
   }
}