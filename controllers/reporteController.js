import Reporte from '../models/reporte'

export default {
    addReporte: async(req,res,next)=>{
      try{
        const body = req.body;
        const reporte = await Reporte.create(body)
        res.status(200).json(reporte)
      }catch(err){
        res.status(500).send({
           message:`Ha ocurrido un error ${err}`
        })
        next(err)
      };
    },
    getReportes: async(req,res,next)=>{
        try{
           const reportes = await Reporte.find({})
           .populate('empresa')
           .populate('encargado')
           .populate('usuario')
           res.status(200).json(reportes)
        }catch(err){
           res.status(500).send({message: `Ocurrio un error ${err}`})
           next(err)
        }
    },
    getReporte: async(req,res,next)=>{
        try{
           const { id } = req.params;
           const reporteID  = await Reporte.findById(id)
           .populate('empresa')
           .populate('encargado')
           .populate('usuario')
           if(!reporteID){
             res.status(404).json({message: 'No existen el reporte'})
           }else{
             res.status(200).json(reporteID)
           }
        }catch(err){
          res.status(500).send({message: `Ocurrio un error ${err}`})
          next(err)
        }
    },
    updateReporte: async(req,res,next)=>{
        try{
          let { id }= req.params;
          const update = req.body;
          const reporte =  await Reporte.findByIdAndUpdate(id,update,{new:true})
          res.status(200).json(reporte)
        }catch(err){
            res.status(500).send({message: `Ocurrio un error ${err}`})
            next(err) 
        }
    },
    deleteReporte: async(req,res,next)=>{
        try{
           const { id } = req.params;
           const reporte = await Reporte.findByIdAndDelete(id)
           res.status(200).json({ok:true,message:'Reporte eliminado con exito'})
        }catch(err){
          res.status(500).send({message: `Ocurrio un error ${err}`})
          next(err)   
        }
    }
}