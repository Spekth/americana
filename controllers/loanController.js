import Loan from '../models/loan'
import Equipment from '../models/equipo'
import pdf from 'html-pdf'

const activateDevice =  async (id) => {
  // const { id } = req.params; 
  const equipment = await Equipment.findByIdAndUpdate(id,{disponibilidad:'true',new:true}); 
  return equipment;  
}
const deactivateDevice =  async (id) => {
  // const { id } = req.params; 
  const equipment = await Equipment.findByIdAndUpdate(id,{disponibilidad:'false',new:true}); 
  return equipment;  
}


const activateLoan =  async (id) => {
  // const { id } = req.params; 
  const loan = await Loan.findByIdAndUpdate(id,{loanState:'active',new:true}); 
  return loan;  
}

const deactivateLoan =  async (id) => {
  // const { id } = req.params; 
  const loan = await Loan.findByIdAndUpdate(id,{loanState:'finished',new:true}); 
  return loan;  
}

export default { 

   addLoan: async (req,res,next)=>{
     try {
        const body = req.body 
        deactivateDevice(req.body.equipo._id)
        const loan = await Loan.create(body);
        res.status(200).send({loan})

     }catch(err){
       res.status(500).send({
         message:`An error ocurred ${err}`
       })
       next(err)
     };
   },

   getLoans: async (req,res,next)=>{
      try{
        const loans = await Loan.find({})
        // .populate([{path:'equipo',path:'usuario'}])
        .populate('usuario',{nombre:1,apellido:1})
        .populate('equipo',{nombre:1,numSerie:1,disponibilidad:1})
        .populate('encargado',{nombre:1,apellido:1})
        res.status(200).send({
          ok:true,
          loans
        })
      } catch(err){
        res.status(500).send({
          message:'An error ocurred'
        })
        next(err) 
      }
   },

   getLoan: async(req,res,next)=>{
     try{
       const { id } = req.params
       const loanID = await Loan.findById(id)
       .populate('encargado')
       .populate('usuario')
       .populate('equipo')
       if(!loanID){
        res.status(404).send({
          message:'There is no loan'
        })
       }else{
         res.status(200).json(loanID)
       }
     }catch(err) {
      res.status(500).send({
        message:'An error ocurred'
      })
      next(err)
     }
   },

   downloadReport: async(req,res,next)=>{
     try{
      const { id } = req.params
      const loanID = await Loan.findById(id)
      .populate('encargado')
      .populate('usuario')
      .populate('equipo')
      let { usuario, encargado,descripcion} = loanID;
      console.log({'usuario':usuario.nombre, 'encargado':encargado.nombre,descripcion});
      let html = `
      <!DOCTYPE html>
      <head>
          <meta charset="UTF-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Google Signin</title>
          <meta name="google-signin-client_id" content="450695550534-76o874njdg8n0k5q6s6i38djqbjutdbk.apps.googleusercontent.com">
          <!-- Bootstrap -->
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      </head>
      <body>
      <div class='container'>
        <div id="pageHeader" style="border-bottom: 1px solid #ddd; padding-bottom: 5px;">
        <img src="https://res.cloudinary.com/dqhme1rod/image/upload/v1625688399/americana/drkg16zitzdbxmc3ybpl.jpg" width="160" height="auto" align="left"> 
         <img src="http://desarrolloweb.com/images/logodwgris.png" width="150" height="27" align="left">
         <p style="color: #666; margin: 0; padding-top: 12px; padding-bottom: 5px; text-align: right; font-family: sans-serif; font-size: .85em">
           Corporación Universitaria americana sede Montería
         </p>
       </div>
        <div class='row'>
          <div class='col-md-9'>
           <h5 class='text-center' id='report' style="font-size: 24px;font-weight:bold">Informe</h5>
          </div>
        </div>
        <div class='row'>
          <div class='col-md-2' style='background-color:"aaa">
          <p id='descripcion' align="justify" style='font-size:14px'>${descripcion}</p>

          </div>
        </div>
        <br><br><br><br><br><br>
        <div id='infoLoan'>
          <p id='encargado' style='text-decoration:overline;color:#022e5a;'>
            <span><strong>Encargado</strong></span>
          </p>

          <p id='empleado' style='color:#022e5a;text-decoration:overline;text-align: right;margin-top:-38px'>
            <span><strong>Empleado</strong></span>
          </p>
        </div>
        <div id="pageFooter" style="border-top: 1px solid #ddd; padding-top: 5px;">
          <p style="color: #666; margin: 0; padding-bottom: 5px; text-align: right; font-family: sans-serif; font-size: .65em">&#169; Corporación Universitaria Americana 2021</p>
        </div>
      </div>
      <script src="https://apis.google.com/js/platform.js" async defer></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
      </body>
      </html>

       
      `;

      if(!loanID){
       res.status(404).send({
         message:'There is no loan'
       })
      }else{
        var options = {
          "format": 'A4',
          "border": {
            "top": "1cm",            // default is 0, units: mm, cm, in, px
            "right": "2cm",
            "bottom": "2cm",
            "left": "2cm"
          },
         };
        console.log(options);
        // pdf.create(html).toFile('html-pdf.pdf', function(err, res) {
        pdf.create(html,options).toFile(`/home/neider/Escritorio/report.pdf`, function(err, res) {
          if (err){
              console.log(err);
          } else {
              console.log(res);
          }
        });
        res.status(200).json(loanID)
      }
     }catch(err){
      res.status(500).send({
        message:'An error ocurred'
      })
      next(err)
     }
   },

   updateLoan: async(req,res,next)=>{
     try{
       const { id } =  req.params;
       const update = req.body;
       const loan = await Loan.findByIdAndUpdate(id,update,{new:true})
       res.status(200).json(loan)
      }catch(err){
       res.status(500).send({
         message:'An error ocurred'
       })
       next(err)
     }
   },
   /*Los encargados no se van a eliminar, unicamente se va
   a cambiar su estado a false;*/
   deleteLoan: async(req,res,next)=>{
     try{
       const { id } = req.params
       let data = await Loan.findById(id)
       .populate('equipo')
       const { _id, nombre, numSerie,disponibilidad} = data.equipo; 
       console.log('_id:',_id, 'nameDevice:',nombre,'numSerieDevice:',numSerie, 'Disponibilidad:',disponibilidad)
       activateDevice(_id);
      //  res.status(200).json({ok:true,data});
        const loan= await Loan.findByIdAndDelete(id)
       res.status(200).json({ok:true,message:'Loan successfully removed'})
     }catch(err){
        res.status(500).send({
          message:'An error ocurred'
        })
        next(err)
     }
   },
   activateDeviceLoan: async(req,res,next)=>{
     try{
       const { id } = req.params
       let deviceDeliver = await Loan.findById(id)
       .populate('equipo')
       const { _id, nombre, numSerie,disponibilidad} = deviceDeliver.equipo; 
      //  console.log('_id:',_id, 'nameDevice:',nombre,'numSerieDevice:',numSerie, 'Disponibilidad:',disponibilidad)
       activateDevice(_id);
       res.status(200).json({ok:true,deviceDeliver});
       
     }catch(err){
        res.status(500).send({
          message:'An error ocurred'
        })
        next(err)
     }
   },
   deActivateLoan: async(req,res,next)=>{
     try{
       const { id } = req.params
       let loanDeactivate = await Loan.findById(id)
       
       .populate('equipo')
       .populate('usuario')
       .populate('encargado')

       let { _id, loanState } = loanDeactivate;
       console.log('LoanState:',loanState,'_id:',_id)

       deactivateLoan(_id);
       res.status(200).json({ok:true,loanDeactivate});
       
     }catch(err){
        res.status(500).send({
          message:'An error ocurred'
        })
        next(err)
     }
   },
   activateLoan: async(req,res,next)=>{
     try{
       const { id } = req.params
       let loanActivate = await Loan.findById(id)
       
       .populate('equipo')
       .populate('usuario')
       .populate('encargado')

       let { _id, loanState } = loanActivate;
       console.log('LoanState:',loanState,'_id:',_id)

       activateLoan(_id);
       res.status(200).json({ok:true,loanActivate});
       
     }catch(err){
        res.status(500).send({
          message:'An error ocurred'
        })
        next(err)
     }
   },
}