require('dotenv').config()
import  Usuario from '../models/usuario'
import  bcrypt  from 'bcrypt'
import  token from '../services/token'



export default { 
  // Create employee
   addUsuario: async (req,res,next)=>{
     try {
        let body = req.body
        body.password = await bcrypt.hashSync(body.password,10);
        const usuario = await Usuario.create(body)
        res.status(200).send({usuario})
     }catch(err){ 
       res.status(500).send({
         message:`An error ocurred ${err}`
       })
       next(err)
     };
   },
   getUsuarios: async (req,res,next)=>{
      try{
        const usuarios = await Usuario.find({})
        res.status(200).send({
          ok:true,
          usuarios
        })
      } catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)       
      }
   },
   getUserEquipment: async (req,res,next)=>{
    
      try{
        const usuarios = await Usuario.find({})
        .populate('equipo')
        
        let infoEquipment = []; 
        let numeroSerie = [];
        for(let {equipo} of usuarios){
          for(let { nombre } of equipo){
            infoEquipment.push(nombre);
          }
        }
        for(let {equipo} of usuarios){
          for(let { numSerie } of equipo){
            numeroSerie.push(numSerie);
          }
        }

        // Imagenes
        let images = [];
        for(let {equipo} of usuarios){
          for(let { image } of equipo){
            images.push(image);
          }
        }
        console.log(images);

        res.status(200).send({
          ok:true,
          infoEquipment,
          numeroSerie,
          images  
        })
        
      } catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)       
      }
   },
   // Get only user 
   getUsuario: async(req,res,next)=>{
     try{
       const { id } = req.params
       const usuarioID = await Usuario.findById(id)
        // .populate('Equipo')
       if(!usuarioID){
        res.status(404).send({
          message:'No existe el usuario'
        })
       }else{
         res.status(200).json(usuarioID)
       }

     }catch(err) {
      res.status(500).send({
        message:'Ocurrio un error '
      })
      next(err)
     }

   },
   updateUsuario: async(req,res,next)=>{
     try{
       const { id } =  req.params;
       const update = req.body;
       let pas = req.body.password;
       const reg0= Usuario.findOne(id)
       if(pas!= reg0.password){
         req.body.password = await bcrypt.hashSync(req.body.password,10)
       }
       const usuario = await Usuario.findByIdAndUpdate(id,update,{new:true})
       .populate('equipo')
       res.status(200).json(usuario)
      }catch(err){
       res.status(500).send({
         message:'Ocurrio un error'
       })
       next(err)
     }
   },
   deleteUsuario: async(req,res,next)=>{
     try{
       const { id } = req.params
       const usuario= await Usuario.findByIdAndDelete(id)
       res.status(200).json({ok:true,message:'Usuario eliminado con exito'})
     }catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)
     }
   }
}
