import Equipo from '../models/equipo'
import Loan from '../models/loan'

export default {
   // Todos los equipos
   getEquipos: async (req,res,next)=>{
      try{
        const equipos = await Equipo.find({})
        
        res.status(200).send({
           ok:true,
           equipos
        })
     

      } catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err) 
      }
   },
   // Get un equipo
   getEquipo: async(req,res,next)=>{
     try{
       const { id } = req.params
       const equipoID = await Equipo.findById(id)

       if(!equipoID){
        res.status(404).send({
          message:'No existe el equipo'
        })
       }else{
        res.status(200).json(equipoID)
       }
     }catch(err) {
      res.status(500).send({
        message:'Ocurrio un error '
      })
      next(err)
     }
   },
   updateEquipo: async(req,res,next)=>{
     try{
       const { id } =  req.params;
       const update = req.body;
       const equipo = await Equipo.findByIdAndUpdate(id,update,{new:true})
       res.status(200).json(equipo)
      }catch(err){
       res.status(500).send({
         message:'Ocurrio un error'
       })
       next(err)
     }
   },
   deleteEquipo: async(req,res,next)=>{
     try{
       const { id } = req.params
       const equipo= await Equipo.findByIdAndDelete(id)
       res.status(200).json({ok:true,message:'Equipo eliminado con exito'})
     }catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)
     }
   },
   desactivarEquipo: async (req,res,next) => {
    try {
        let { id } = req.params;
        let update = req.body;  
        const equipo = await Equipo.findByIdAndUpdate(id,{disponibilidad:'false',new:true}); 
        res.status(200).json(equipo); 
    } catch(e){
        res.status(500).send({
            message:`Ocurrió un error ${e}`
        });
        next(e);
    }
},
   activarEquipo: async (req,res,next) => {
    try {
        let { id } = req.params;
        let update = req.body;  
        console.log(req.body);
        const equipo = await Equipo.findByIdAndUpdate(id,{disponibilidad:'true',new:true});
        
        res.status(200).json(equipo); 
    } catch(e){
        res.status(500).send({
            message:`Ocurrió un error ${e}`
        });
        next(e);
    }
},
}