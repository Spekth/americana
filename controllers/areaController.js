import Area from '../models/area'

export default { 
  // Create Area 
   addArea: async (req,res,next)=>{
     try {
        const body = req.body
        const area = await Area.create(body)
        res.status(200).send({area})
     }catch(err){
       res.status(500).send({
         message:`An error ocurred ${err}`
       })
       next(err)
     };
   },
   // Todas las areas
   getAreas: async (req,res,next)=>{
      try{
        const areas = await Area.find({})
         .populate('encargado')
        res.status(200).send({
           ok:true,
           areas 
        })
      } catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err) 
      }
   },
   // Obtener un area 
   getArea: async(req,res,next)=>{
     try{
       const { id } = req.params
       const areaID = await Area.findById(id)
        .populate('encargado')
       if(!areaID){
        res.status(404).send({
          message:'No existe el area'
        })
       }else{
         res.status(200).json(areaID)
       }
     }catch(err) {
      res.status(500).send({
        message:'Ocurrio un error '
      })
      next(err)
     }
   },
   // Actualizar el area
   updateArea: async(req,res,next)=>{
     try{
       const { id } =  req.params;
       const update = req.body;
       const area = await Area.findByIdAndUpdate(id,update,{new:true})
       res.status(200).json(area)
      }catch(err){
       res.status(500).send({
         message:'Ocurrio un error'
       })
       next(err)
     }
   },
   // Eliminar area
   deleteArea: async(req,res,next)=>{
     try{
       const { id } = req.params
       const area= await Area.findByIdAndDelete(id)
       res.status(200).json({ok:true,message:'Area eliminada con exito'})
     }catch(err){
        res.status(500).send({
          message:'Ocurrio un error'
        })
        next(err)
     }
   }
}