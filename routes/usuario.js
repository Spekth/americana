import express from 'express'
import usuarioController from '../controllers/usuarioController'
import { login } from '../controllers/auth'
import {
  getRestorePasswordUrl,
  restorePassword,
  verifyPasswordRestore
 } from '../helpers/restorePass'
import auth from '../middlewares/auth'
import  validarCampos  from '../middlewares/validar-campos'

// express-validator
import { check } from 'express-validator'

const router = express.Router()

router.get('/usuarios',auth.verifyAdministrador, usuarioController.getUsuarios)
router.get('/usuario/:id',auth.verifyAdministrador, usuarioController.getUsuario)
router.get('/info-user-equipment',auth.verifyAdministrador,usuarioController.getUserEquipment)
router.put('/usuario/:id',/* auth.verifyAdministrador, */ usuarioController.updateUsuario)
router.delete('/usuario/:id',auth.verifyAdministrador, usuarioController.deleteUsuario)

// SEND EMAIL WITH VALID EMAIL BY RESTORE PASSWORD
router.post('/forgot-password',getRestorePasswordUrl);

// GET EMAIL URL BY RESTORE PASSWORD
router.post('/password-reset/:userId/:token',restorePassword);

router.post('/login',login)
export default router;

