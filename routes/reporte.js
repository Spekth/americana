import express from 'express'
import reporteController from '../controllers/reporteController'

const router = express.Router()

router.post('/reporte',/*auth.verifyAdministrador,*/reporteController.addReporte)
// router.get('/reportes',/*auth.verifyAdministrador,*/reporteController.getReportes)
router.get('/reporte/:id',/*auth.verifyAdministrador,*/reporteController.getReporte)
router.put('/reporte/:id',/*auth.verifyAdministrador,*/reporteController.updateReporte)
router.delete('/reporte/:id',/*auth.verifyAdministrador,*/reporteController.deleteReporte)



export default router