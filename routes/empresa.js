import express from 'express'
import empresaController from '../controllers/empresaController'
import auth from '../middlewares/auth';

const router = express.Router()

router.get('/empresa',auth.verifyAdministrador,empresaController.getEmpresas)
router.get('/empresa/:id',auth.verifyAdministrador, empresaController.getEmpresa)
router.put('/empresa/:id',auth.verifyAdministrador,empresaController.updateEmpresa)
router.delete('/empresa/:id',auth.verifyAdministrador, empresaController.deleteEmpresa)

export default router;