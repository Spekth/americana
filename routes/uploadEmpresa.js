require('dotenv').config(); 
require('../config/cloudinaryConfig');  
import auth from '../middlewares/auth';
import express from 'express';
const app = express(); 
import multer from 'multer';  
import Empresa from '../models/empresa';
const cloudinary = require('cloudinary').v2; 
const storage = multer({
  storage: multer.diskStorage({}), 
  fileFilter: (req, file, cb) => {
    if (!file.mimetype.match(/jpe|jpeg|png|gif$i/)) {
      cb(new Error('El archivo no es soportado'), false)
      // return 
    }
    cb(null, true)
  }
});

app.post('/empresa',auth.verifyAdministrador, storage.single('logo'),async (req, res,next) => {
  try {   
    let body = req.body; 
    const result = await cloudinary.uploader.upload(req.file.path ,{folder:'americana/'});
    body.logo = result.secure_url;
    const empresa = await Empresa.create(body); 
    res.status(200).json({empresa});
    console.log(result);
  }catch(err){   
    res.status(500).send({  
       message: `An ocurred error ${err}`   
    })   
    next(err);  
  }           
});

/* Update Image */

app.put('/empresa/logo/:id',auth.verifyAdministrador, storage.single('logo'),async (req, res,next) => {
  try {

    const { id } =  req.params;
    const update = req.body;
    /* </Update todos los campos de empresa> */
    
    if(!req.file){
      let empresa = await Empresa.findById(id);
      const { nombre,nit,celular,email,direccion } = update
      
      let data = { 
        logo: empresa.logo,
        nombre,
        nit,
        celular,
        email,
        direccion
        
      }
      
      const emp = await Empresa.findByIdAndUpdate(id,data,{new:true});
      res.json(emp)
    
    }else{
      const result = await cloudinary.uploader.upload(req.file.path,{folder:'americana/'});
      const data = {
        logo: result.secure_url,
        ...update
      };
      const emp = await Empresa.findByIdAndUpdate(id,data,{new:true})
      res.json(emp)
  
    }

  }catch(err){   
    res.status(500).send({  
       message: `An ocurred error ${err}`   
    })   
    next(err);  
  }           
});

export default app
