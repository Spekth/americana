require('dotenv').config(); 
require('../config/cloudinaryConfig');
import auth from '../middlewares/auth';  
import express from 'express';
const app = express(); 
import multer from 'multer';  
import User from '../models/usuario';
import bcrypt from 'bcrypt';
const cloudinary = require('cloudinary').v2; 
const storage = multer({
  storage: multer.diskStorage({}), 
  fileFilter: (req, file, cb) => {
    if (!file.mimetype.match(/jpe|jpeg|png|gif$i/)) {
      cb(new Error('El archivo no es soportado'), false)
      // return 
    }
    cb(null, true)
  }
})

app.post('/user',auth.verifyAdministrador, storage.single('image'),async (req, res,next) => {
  // Code ES6
  try {   
    let body = req.body; 
    body.password = await bcrypt.hashSync(body.password,10);
    const result = await cloudinary.uploader.upload(req.file.path,{folder:'americana/'});
    body.image = result.secure_url;
    const user = await User.create(body); 
    res.status(200).json({user});

  }catch(err){   
    res.status(500).send({  
       message: `An ocurred error ${err}`   
    })   
    next(err);  
  }           
});

/* Update Image */

app.put('/user/image/:id',auth.verifyAdministrador, storage.single('image'),async (req, res,next) => {
  try {
    let user = await User.findById(req.params.id);
    const result = await cloudinary.uploader.upload(req.file.path,{folder:'americana/'});
    const data = {
      image: result.secure_url || user.image,

    };
    user = await User.findByIdAndUpdate(req.params.id, data, {new: true });
    res.json(user);

  }catch(err){   
    res.status(500).send({  
       message: `An ocurred error ${err}`   
    })   
    next(err);  
  }           
});
/* Update Image */

export default app