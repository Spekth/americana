require('dotenv').config(); 
require('../config/cloudinaryConfig');
import auth from '../middlewares/auth';  
import express from 'express';
const app = express(); 
import multer from 'multer';  
import Equipo from '../models/equipo';
const cloudinary = require('cloudinary').v2; 
const storage = multer({
  storage: multer.diskStorage({}), 
  fileFilter: (req, file, cb) => {
    if (!file.mimetype.match(/jpe|jpeg|png|gif$i/)) {
      cb(new Error('El archivo no es soportado'), false)
      // return 
    }
    cb(null, true)
  }
})

app.post('/equipo',auth.verifyAdministrador, storage.single('image'),async (req, res,next) => {
  // Code ES6
  try {   
    let body = req.body; 
    const result = await cloudinary.uploader.upload(req.file.path,{folder:'americana/'});
    body.image = result.secure_url;
    const equipo = await Equipo.create(body); 
    res.status(200).json({equipo});

  }catch(err){   
    res.status(500).send({  
       message: `An ocurred error ${err}`   
    })   
    next(err);  
  }           
});

/* Update Image */

app.put('/equipo/image/:id',auth.verifyAdministrador, storage.single('image'),async (req, res,next) => {
  try {
    const { id } = req.params;
    const update = req.body;

    if(!req.file){
      let equipo = await Equipo.findById(id);
      const { nombre, numSerie, estado, fechaIngreso, fechaEgreso, disponibilidad } = update;

      let data = {
        image: equipo.image,
        nombre,
        numSerie,
        estado,
        fechaIngreso,
        fechaEgreso,
        disponibilidad
      }
      const eq = await Equipo.findByIdAndUpdate(id,data,{new:true});
      res.json(eq);
    }else{
      const result = await cloudinary.uploader.upload(req.file.path,{folder:'americana/'});
      const data = {
        image: result.secure_url,
        ...update 
      };
      const eq = await Equipo.findByIdAndUpdate(id,data,{new:true});
      res.json(eq);
    }

  }catch(err){   
    res.status(500).send({  
       message: `An ocurred error ${err}`   
    })   
    next(err);  
  }           
});
/* Update Image */

export default app