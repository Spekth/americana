import express from 'express'
import equipoController from '../controllers/equipoController'
import auth from '../middlewares/auth';

const router = express.Router()

router.get('/equipos',auth.verifyAdministrador,equipoController.getEquipos)
router.get('/equipo/:id',auth.verifyAdministrador,equipoController.getEquipo)
router.put('/equipo/:id',auth.verifyAdministrador,equipoController.updateEquipo)
router.delete('/equipo/:id',auth.verifyAdministrador,equipoController.deleteEquipo)


router.put('/equipo/desactivar/:id',auth.verifyAdministrador,equipoController.desactivarEquipo)
router.put('/equipo/activar/:id',auth.verifyAdministrador,equipoController.activarEquipo)


export default router;