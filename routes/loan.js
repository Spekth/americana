import express from 'express'
import loanController from '../controllers/loanController'
import auth from '../middlewares/auth';

const router = express.Router()

router.get('/prestamos',auth.verifyAdministrador,loanController.getLoans)
router.get('/prestamo/:id',auth.verifyAdministrador,loanController.getLoan)

router.get('/prest/:id',/* auth.verifyAdministrador, */loanController.downloadReport)

router.post('/prestamo',auth.verifyAdministrador,loanController.addLoan)
router.put('/prestamo/:id',auth.verifyAdministrador,loanController.updateLoan)
router.delete('/prestamo/:id',auth.verifyAdministrador,loanController.deleteLoan)
router.get('/activate-device-loan/:id',auth.verifyAdministrador,loanController.activateDeviceLoan);

router.get('/deactivate-loan/:id',auth.verifyAdministrador,loanController.deActivateLoan);
router.get('/activate-loan/:id',auth.verifyAdministrador,loanController.activateLoan);


export default router;