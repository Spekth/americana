# API- Corporación Universitaría Amerciana sede Montería.

Esta es la API de una aplicación web para la gestión y prestamo de equipos tecnologicos de la Corporación Universitaría Americana - Sede Montería.

El BackEnd se desarrollo en `NodeJS` con el framework `ExpressJS`, para luego ser consumida por un FrontEnd desarrollado en `VueJS`,`Bootstrap` y `BootstrapVue`. Este proyecto se desarrollo con el fin de suplir una necesidad que se estaba presentando en la institución. El registro de los equipos que se compraban y el prestamo interno de los mismos se estaba llevando de forma manual, gracias a este proyecto se pudo digitalizar este proceso para llevar un mejor control del mismo. 

La supervisión total mientras se estuvo desarrollando el proyecto estuvo a cargo de un docente de sistemas asignado, quien fue el responsable de verificar que todas las funcionalidades estuviesen en orden y que fuesen  eficientes.  

Para reconstruir los modulos de Node ejecutar

```
npm install 
```